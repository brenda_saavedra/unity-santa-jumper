﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameplayController : MonoBehaviour {

	public static GameplayController instance;

	[SerializeField]
	private GameObject platform;

	[SerializeField]
	private GameObject panel;

	[SerializeField]
	private Text scoreText;

	private float DISTANCE_BETWEEN_PLATFORMS = 4.1f;
	private int countPlatforms;
	private float lastPlatformPositionY;

	// Use this for initialization
	void Awake () {
		panel.SetActive (false);
		MakeSingleton ();
		CreatePlarform ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnDisable(){
		instance = null;
	}

	private void MakeSingleton(){
		if (instance == null) {
			instance = this;
		}
	}

	public void CreatePlarform(){
		lastPlatformPositionY += DISTANCE_BETWEEN_PLATFORMS;

		GameObject newPlatform = Instantiate (platform);
		newPlatform.transform.position = new Vector3 (0, lastPlatformPositionY, 0);
		newPlatform.name = "Platform " + countPlatforms;

		countPlatforms++;
	}

	public void PlayAgain(){
		Time.timeScale = 1;
		SceneManager.LoadScene (SceneManager.GetActiveScene().name);
	}

	public void GoToMenu(){
		Time.timeScale = 1;
		SceneManager.LoadScene ("MainMenu");
	}

	public void enablePanel(){
		panel.SetActive (true);
	}

	public void setScore(int score){
		scoreText.text = score.ToString ();
	}
}
