﻿using UnityEngine;
using System.Collections;

public class PlatformCollector : MonoBehaviour {

	
	void OnTriggerEnter2D(Collider2D target){
		if (target.tag == "Platform") {
			target.gameObject.SetActive (false);
		}

		if (target.tag == "Player") {
			Time.timeScale = 0f;
			GameplayController.instance.enablePanel ();
		}
	}
}
