﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerScript : MonoBehaviour {

	private Rigidbody2D myBody;
	private Button jumpBtn;
	private GameObject parent;
	private bool hasJumped, platformBound;

	public delegate void MoveCamera ();
	public static event MoveCamera move;

	private int score = 0;

	void Awake () {
		jumpBtn = GameObject.Find("Jump Button").GetComponent<Button>();
		jumpBtn.onClick.AddListener (() => Jump ());

		myBody = GetComponent<Rigidbody2D> ();

	}

	void Start(){
		GameplayController.instance.setScore (score);
	}
	
	// Update is called once per frame
	void Update () {
		if (hasJumped && myBody.velocity.y == 0) {
			if (!platformBound) {
				hasJumped = false;
				transform.SetParent (parent.transform);

				score++;
				GameplayController.instance.CreatePlarform ();
				GameplayController.instance.setScore (score);

				if (move != null) {
					move ();
				}
			}else if (parent != null){
				transform.SetParent (parent.transform);
			}
		}
	}

	public void Jump(){
		if (myBody.velocity.y == 0) {
			myBody.velocity = new Vector2 (0, 10);
			transform.SetParent (null);
			hasJumped = true;
		}
	}

	void OnCollisionEnter2D(Collision2D target){
		if (target.gameObject.tag == "Platform") {
			parent = target.gameObject;
		}
	}

	void OnCollisionExit2D(Collision2D target){
		if (target.gameObject.tag == "Platform") {
			parent = null;
		}
	}

	void OnTriggerEnter2D(Collider2D target){
		if (target.tag == "MainCamera") {
			platformBound = true;
		}
	}

	void OnTriggerExit2D(Collider2D target){
		if (target.tag == "MainCamera") {
			platformBound = false;
		}
	}
}
