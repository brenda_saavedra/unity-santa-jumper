﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour {

	private float speed = 1f;

	private float boundLeft = -1.6f, boundRight = 1.6f;

	private bool left;

	// Use this for initialization
	void Awake () {
		randomizeMovement ();
	}
	
	// Update is called once per frame
	void Update () {
		move ();
	}

	private void randomizeMovement(){
		if (Random.Range (0, 2) == 0) {
			left = true;
		} else {
			left = false;
		}
	}

	private void move(){
		Vector3 temp = transform.position;
		if (left) {
			temp.x -= speed * Time.deltaTime;
			transform.position = temp;

			if (transform.position.x < boundLeft) {
				left = false;
			}
		} else {
			temp.x += speed * Time.deltaTime;
			transform.position = temp;

			if (transform.position.x > boundRight) {
				left = true;
			}
		}
	}
}
